include "TriggerLibs/NativeLib"

//--------------------------------------------------------------------------------------------------
// Library: Weather
//--------------------------------------------------------------------------------------------------
// Structures
struct lib197EC33E_gs_Weather {
    actor[76] lv_stormDoodads;
    actor[16] lv_sFXDoodads;
    int lv_stormDuration;
    int lv_clearSkiesDuration;
    bool lv_weatherStop;
};

// Variables
lib197EC33E_gs_Weather[12] lib197EC33E_gv_weather;

void lib197EC33E_InitVariables () {
    int init_i;
    int init_i1;

    init_i = 0;
    while (init_i <= 11) {
        init_i1 = 0;
        while (init_i1 <= 75) {
            lib197EC33E_gv_weather[init_i].lv_stormDoodads[init_i1] = null;
            init_i1 = init_i1 + 1;
        }
        init_i1 = 0;
        while (init_i1 <= 15) {
            lib197EC33E_gv_weather[init_i].lv_sFXDoodads[init_i1] = null;
            init_i1 = init_i1 + 1;
        }
        lib197EC33E_gv_weather[init_i].lv_stormDuration = 0;
        lib197EC33E_gv_weather[init_i].lv_clearSkiesDuration = 0;
        lib197EC33E_gv_weather[init_i].lv_weatherStop = false;
        init_i = init_i + 1;
    }
}

// Function Declarations
void lib197EC33E_gf_Weather (int lp_weatherType, int lp_weatherIntensity, region lp_weatherRegion, int lp_stormID, int lp_stormMinDuration, int lp_stormMaxDuration, int lp_clearSkiesMinDuration, int lp_clearSkiesMaxDuration);
void lib197EC33E_gf_WeatherStop (int lp_stormID);
void lib197EC33E_gf_ActivateDeactivateSFX (int lp_weatherType, int lp_weatherIntensity, int lp_weatherSFXActivateDeactivate, int lp_stormID, region lp_weatherRegion);
int lib197EC33E_gf_WeatherIntensityGuage (int lp_weatherIntensity);
string lib197EC33E_gf_WeatherSFXEmiter (int lp_weatherType, int lp_sFXActorIndex);
string lib197EC33E_gf_WeatherEmiter (int lp_weatherType, int lp_actorIndex);

// Functions
trigger auto_lib197EC33E_gf_Weather_Trigger = null;
int auto_lib197EC33E_gf_Weather_lp_weatherType;
int auto_lib197EC33E_gf_Weather_lp_weatherIntensity;
region auto_lib197EC33E_gf_Weather_lp_weatherRegion;
int auto_lib197EC33E_gf_Weather_lp_stormID;
int auto_lib197EC33E_gf_Weather_lp_stormMinDuration;
int auto_lib197EC33E_gf_Weather_lp_stormMaxDuration;
int auto_lib197EC33E_gf_Weather_lp_clearSkiesMinDuration;
int auto_lib197EC33E_gf_Weather_lp_clearSkiesMaxDuration;

void lib197EC33E_gf_Weather (int lp_weatherType, int lp_weatherIntensity, region lp_weatherRegion, int lp_stormID, int lp_stormMinDuration, int lp_stormMaxDuration, int lp_clearSkiesMinDuration, int lp_clearSkiesMaxDuration) {
    auto_lib197EC33E_gf_Weather_lp_weatherType = lp_weatherType;
    auto_lib197EC33E_gf_Weather_lp_weatherIntensity = lp_weatherIntensity;
    auto_lib197EC33E_gf_Weather_lp_weatherRegion = lp_weatherRegion;
    auto_lib197EC33E_gf_Weather_lp_stormID = lp_stormID;
    auto_lib197EC33E_gf_Weather_lp_stormMinDuration = lp_stormMinDuration;
    auto_lib197EC33E_gf_Weather_lp_stormMaxDuration = lp_stormMaxDuration;
    auto_lib197EC33E_gf_Weather_lp_clearSkiesMinDuration = lp_clearSkiesMinDuration;
    auto_lib197EC33E_gf_Weather_lp_clearSkiesMaxDuration = lp_clearSkiesMaxDuration;

    if (auto_lib197EC33E_gf_Weather_Trigger == null) {
        auto_lib197EC33E_gf_Weather_Trigger = TriggerCreate("auto_lib197EC33E_gf_Weather_TriggerFunc");
    }

    TriggerExecute(auto_lib197EC33E_gf_Weather_Trigger, false, false);
}

bool auto_lib197EC33E_gf_Weather_TriggerFunc (bool testConds, bool runActions) {
    int lp_weatherType = auto_lib197EC33E_gf_Weather_lp_weatherType;
    int lp_weatherIntensity = auto_lib197EC33E_gf_Weather_lp_weatherIntensity;
    region lp_weatherRegion = auto_lib197EC33E_gf_Weather_lp_weatherRegion;
    int lp_stormID = auto_lib197EC33E_gf_Weather_lp_stormID;
    int lp_stormMinDuration = auto_lib197EC33E_gf_Weather_lp_stormMinDuration;
    int lp_stormMaxDuration = auto_lib197EC33E_gf_Weather_lp_stormMaxDuration;
    int lp_clearSkiesMinDuration = auto_lib197EC33E_gf_Weather_lp_clearSkiesMinDuration;
    int lp_clearSkiesMaxDuration = auto_lib197EC33E_gf_Weather_lp_clearSkiesMaxDuration;

    // Implementation
    if ((lib197EC33E_gv_weather[lp_stormID].lv_weatherStop == true)) {
        lib197EC33E_gv_weather[lp_stormID].lv_weatherStop = false;

    }
    else {
        Wait(1.0, c_timeReal);
        lib197EC33E_gv_weather[lp_stormID].lv_stormDuration = RandomInt(lp_stormMinDuration, lp_stormMaxDuration);
        lib197EC33E_gv_weather[lp_stormID].lv_clearSkiesDuration = RandomInt(lp_clearSkiesMinDuration, lp_clearSkiesMaxDuration);
        lib197EC33E_gf_ActivateDeactivateSFX(lp_weatherType, lp_weatherIntensity, 0, lp_stormID, lp_weatherRegion);
        IntLoopBegin(0, lib197EC33E_gf_WeatherIntensityGuage(lp_weatherIntensity));
        while (!IntLoopDone()) {
            libNtve_gf_CreateActorAtPoint(lib197EC33E_gf_WeatherEmiter(lp_weatherType, IntLoopCurrent()), RegionRandomPoint(lp_weatherRegion));
            lib197EC33E_gv_weather[lp_stormID].lv_stormDoodads[IntLoopCurrent()] = libNtve_gf_ActorLastCreated();
            ActorSend(lib197EC33E_gv_weather[lp_stormID].lv_stormDoodads[IntLoopCurrent()], "SetOpacity 0.000000 0.010000");
            Wait(0.05, c_timeReal);
            IntLoopStep();
        }
        IntLoopEnd();
        IntLoopBegin(0, lib197EC33E_gf_WeatherIntensityGuage(lp_weatherIntensity));
        while (!IntLoopDone()) {
            ActorSend(lib197EC33E_gv_weather[lp_stormID].lv_stormDoodads[IntLoopCurrent()], "SetOpacity 1.000000 3.000000");
            Wait((lib197EC33E_gv_weather[lp_stormID].lv_stormDuration / lib197EC33E_gf_WeatherIntensityGuage(lp_weatherIntensity)), c_timeReal);
            IntLoopStep();
        }
        IntLoopEnd();
        Wait(lib197EC33E_gv_weather[lp_stormID].lv_stormDuration, c_timeReal);
        IntLoopBegin(0, lib197EC33E_gf_WeatherIntensityGuage(lp_weatherIntensity));
        while (!IntLoopDone()) {
            ActorSend(lib197EC33E_gv_weather[lp_stormID].lv_stormDoodads[IntLoopCurrent()], "SetOpacity 0.000000 3.000000");
            Wait((lib197EC33E_gv_weather[lp_stormID].lv_clearSkiesDuration / lib197EC33E_gf_WeatherIntensityGuage(lp_weatherIntensity)), c_timeReal);
            IntLoopStep();
        }
        IntLoopEnd();
        lib197EC33E_gf_ActivateDeactivateSFX(lp_weatherType, lp_weatherIntensity, 1, lp_stormID, lp_weatherRegion);
        IntLoopBegin(0, lib197EC33E_gf_WeatherIntensityGuage(lp_weatherIntensity));
        while (!IntLoopDone()) {
            ActorSend(lib197EC33E_gv_weather[lp_stormID].lv_stormDoodads[IntLoopCurrent()], "Destroy Immediate");
            Wait((lib197EC33E_gv_weather[lp_stormID].lv_clearSkiesDuration / lib197EC33E_gf_WeatherIntensityGuage(lp_weatherIntensity)), c_timeReal);
            IntLoopStep();
        }
        IntLoopEnd();
        Wait(lib197EC33E_gv_weather[lp_stormID].lv_clearSkiesDuration, c_timeReal);
        lib197EC33E_gf_Weather(lp_weatherType, lp_weatherIntensity, lp_weatherRegion, lp_stormID, lp_stormMinDuration, lp_stormMaxDuration, lp_clearSkiesMinDuration, lp_clearSkiesMaxDuration);

    }
    return true;
}

void lib197EC33E_gf_WeatherStop (int lp_stormID) {
    // Implementation
    lib197EC33E_gv_weather[lp_stormID].lv_weatherStop = true;
}

trigger auto_lib197EC33E_gf_ActivateDeactivateSFX_Trigger = null;
int auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_weatherType;
int auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_weatherIntensity;
int auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_weatherSFXActivateDeactivate;
int auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_stormID;
region auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_weatherRegion;

void lib197EC33E_gf_ActivateDeactivateSFX (int lp_weatherType, int lp_weatherIntensity, int lp_weatherSFXActivateDeactivate, int lp_stormID, region lp_weatherRegion) {
    auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_weatherType = lp_weatherType;
    auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_weatherIntensity = lp_weatherIntensity;
    auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_weatherSFXActivateDeactivate = lp_weatherSFXActivateDeactivate;
    auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_stormID = lp_stormID;
    auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_weatherRegion = lp_weatherRegion;

    if (auto_lib197EC33E_gf_ActivateDeactivateSFX_Trigger == null) {
        auto_lib197EC33E_gf_ActivateDeactivateSFX_Trigger = TriggerCreate("auto_lib197EC33E_gf_ActivateDeactivateSFX_TriggerFunc");
    }

    TriggerExecute(auto_lib197EC33E_gf_ActivateDeactivateSFX_Trigger, false, false);
}

bool auto_lib197EC33E_gf_ActivateDeactivateSFX_TriggerFunc (bool testConds, bool runActions) {
    int lp_weatherType = auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_weatherType;
    int lp_weatherIntensity = auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_weatherIntensity;
    int lp_weatherSFXActivateDeactivate = auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_weatherSFXActivateDeactivate;
    int lp_stormID = auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_stormID;
    region lp_weatherRegion = auto_lib197EC33E_gf_ActivateDeactivateSFX_lp_weatherRegion;

    int auto9EBF665F_val;

    // Implementation
    auto9EBF665F_val = lp_weatherSFXActivateDeactivate;
    if (auto9EBF665F_val == 0) {
        IntLoopBegin(0, (lib197EC33E_gf_WeatherIntensityGuage(lp_weatherIntensity) / 5));
        while (!IntLoopDone()) {
            libNtve_gf_CreateActorAtPoint(lib197EC33E_gf_WeatherSFXEmiter(lp_weatherType, IntLoopCurrent()), RegionRandomPoint(lp_weatherRegion));
            lib197EC33E_gv_weather[lp_stormID].lv_sFXDoodads[IntLoopCurrent()] = libNtve_gf_ActorLastCreated();
            Wait(0.05, c_timeGame);
            IntLoopStep();
        }
        IntLoopEnd();
    }
    else if (auto9EBF665F_val == 1) {
        IntLoopBegin(0, (lib197EC33E_gf_WeatherIntensityGuage(lp_weatherIntensity) / 5));
        while (!IntLoopDone()) {
            ActorSend(lib197EC33E_gv_weather[lp_stormID].lv_sFXDoodads[IntLoopCurrent()], "SoundSetVolume Linear 1.000000@0.000000,0.000000@68.181816");
            IntLoopStep();
        }
        IntLoopEnd();
        Wait(1.0, c_timeGame);
        IntLoopBegin(0, (lib197EC33E_gf_WeatherIntensityGuage(lp_weatherIntensity) / 5));
        while (!IntLoopDone()) {
            ActorSend(lib197EC33E_gv_weather[lp_stormID].lv_sFXDoodads[IntLoopCurrent()], "Destroy");
            IntLoopStep();
        }
        IntLoopEnd();

    }
    else {
    }
    return true;
}

int lib197EC33E_gf_WeatherIntensityGuage (int lp_weatherIntensity) {
    int auto36D9E1BB_val;

    // Implementation
    auto36D9E1BB_val = lp_weatherIntensity;
    if (auto36D9E1BB_val == 0) {
        return 25;
    }
    else if (auto36D9E1BB_val == 1) {
        return 50;
    }
    else if (auto36D9E1BB_val == 2) {
        return 75;
    }
    else {
        return 25;
    }
}

string lib197EC33E_gf_WeatherSFXEmiter (int lp_weatherType, int lp_sFXActorIndex) {
    int auto8E096D06_val;

    // Implementation
    auto8E096D06_val = lp_weatherType;
    if (auto8E096D06_val == 3) {
        if (((lp_sFXActorIndex >= 0) && (lp_sFXActorIndex <= 10))) {
            return "MUTE";
        }
        else {
            if (((lp_sFXActorIndex >= 11) && (lp_sFXActorIndex <= 15))) {
                return "Wind";
            }
            else {
                return "MUTE";
            }
        }
    }
    else if (auto8E096D06_val == 1) {
        if (((lp_sFXActorIndex >= 0) && (lp_sFXActorIndex <= 15))) {
            return "Wind";
        }
        else {
            return "MUTE";
        }
    }
    else if (auto8E096D06_val == 0) {
        if (((lp_sFXActorIndex >= 0) && (lp_sFXActorIndex <= 10))) {
            return "Rain";
        }
        else {
            if (((lp_sFXActorIndex >= 11) && (lp_sFXActorIndex <= 15))) {
                return "Thunder";
            }
            else {
                return "MUTE";
            }
        }
    }
    else if (auto8E096D06_val == 2) {
        if (((lp_sFXActorIndex >= 0) && (lp_sFXActorIndex <= 15))) {
            return "WinterWind";
        }
        else {
            return "MUTE";
        }
    }
    else {
        return "MUTE";
    }
}

string lib197EC33E_gf_WeatherEmiter (int lp_weatherType, int lp_actorIndex) {
    int auto02E93FC1_val;

    // Implementation
    auto02E93FC1_val = lp_weatherType;
    if (auto02E93FC1_val == 3) {
        if (((lp_actorIndex >= 0) && (lp_actorIndex <= 50))) {
            return "Sunrays";
        }
        else {
            if (((lp_actorIndex >= 51) && (lp_actorIndex <= 75))) {
                return "WindRough";
            }
            else {
                return "DustLeaves";
            }
        }
    }
    else if (auto02E93FC1_val == 1) {
        if (((lp_actorIndex >= 0) && (lp_actorIndex <= 25))) {
            return "DustStorms";
        }
        else {
            if (((lp_actorIndex >= 26) && (lp_actorIndex <= 50))) {
                return "WindRough";
            }
            else {
                if (((lp_actorIndex >= 51) && (lp_actorIndex <= 75))) {
                    return "DustStorms";
                }
                else {
                    return "DustLeaves";
                }
            }
        }
    }
    else if (auto02E93FC1_val == 0) {
        if (((lp_actorIndex >= 0) && (lp_actorIndex <= 25))) {
            return "FogMist";
        }
        else {
            if (((lp_actorIndex >= 26) && (lp_actorIndex <= 50))) {
                return "RainSprinkle";
            }
            else {
                if (((lp_actorIndex >= 51) && (lp_actorIndex <= 75))) {
                    return "RainEmitterLarge";
                }
                else {
                    return "DustLeaves";
                }
            }
        }
    }
    else if (auto02E93FC1_val == 2) {
        if (((lp_actorIndex >= 0) && (lp_actorIndex <= 39))) {
            return "Snow";
        }
        else {
            if (((lp_actorIndex >= 40) && (lp_actorIndex <= 65))) {
                return "RainSprinkle";
            }
            else {
                if (((lp_actorIndex >= 66) && (lp_actorIndex <= 75))) {
                    return "Blizzard";
                }
                else {
                    return "DustLeaves";
                }
            }
        }
    }
    else {
        return "DustLeaves";
    }
}

//--------------------------------------------------------------------------------------------------
// Library Initialization
//--------------------------------------------------------------------------------------------------
bool lib197EC33E_InitLib_completed = false;

void lib197EC33E_InitLib () {
    if (lib197EC33E_InitLib_completed) {
        return;
    }

    lib197EC33E_InitVariables();

    lib197EC33E_InitLib_completed = true;
}

