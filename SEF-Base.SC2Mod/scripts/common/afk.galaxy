fixed SF_AFK_TIME_LIMIT = 60.0;

struct sf_player_inactive_desc_t {
    bool inactive;
    fixed last_action_time;
    point last_camera_pos;
    playergroup pg_shared_ctrl;
};

static sf_player_inactive_desc_t[c_maxPlayers] pinactive_status;

void sf_afk_action_performed(int player, bool hard_action)
{
    text msg;
    int i;
    int tmp_player;

    pinactive_status[player].last_action_time = GameGetMissionTime();

    if (pinactive_status[player].inactive && hard_action) {
        TextExpressionSetToken("SF/Text/AFK_ControlDisabled", "Player", sf_player_name_colored(player));
        msg = TextExpressionAssemble("SF/Text/AFK_ControlDisabled");
        UIDisplayMessage(
            PlayerGroupActive(),
            c_messageAreaSubtitle,
            msg
        );

        for (i = 1; i <= PlayerGroupCount(pinactive_status[player].pg_shared_ctrl); i += 1) {
            tmp_player = PlayerGroupPlayer(pinactive_status[player].pg_shared_ctrl, i);
            PlayerSetAlliance(player, c_allianceIdControl, tmp_player, false);
        }

        pinactive_status[player].inactive = false;
        pinactive_status[player].pg_shared_ctrl = null;
    }
}

void sf_afk_check_inactive()
{
    int i;
    int l;
    int player;
    int tmp_player;
    fixed inactivity_time;
    text msg;

    for (i = 1; i <= PlayerGroupCount(PlayerGroupActive()); i += 1) {
        player = PlayerGroupPlayer(PlayerGroupActive(), i);
        if (PlayerType(player) != c_playerTypeUser) { continue; }
        if (pinactive_status[player].inactive) { continue; }

        if (pinactive_status[player].last_camera_pos != CameraGetTarget(player)) {
            pinactive_status[player].last_camera_pos = CameraGetTarget(player);
            sf_afk_action_performed(player, false);
        }

        inactivity_time = GameGetMissionTime() - pinactive_status[player].last_action_time;
        if (inactivity_time >= SF_AFK_TIME_LIMIT) {
            TextExpressionSetToken("SF/Text/AFK_ControlEnabled", "Player", sf_player_name_colored(player));
            TextExpressionSetToken("SF/Text/AFK_ControlEnabled", "Seconds", TextTimeFormat(StringToText("<sectotal/>"), FixedToInt(inactivity_time)));
            msg = TextExpressionAssemble("SF/Text/AFK_ControlEnabled");
            UIDisplayMessage(
                PlayerGroupActive(),
                c_messageAreaSubtitle,
                msg
            );

            pinactive_status[player].inactive = true;
            pinactive_status[player].pg_shared_ctrl = PlayerGroupEmpty();

            for (l = 1; l <= PlayerGroupCount(PlayerGroupActive()); l += 1) {
                tmp_player = PlayerGroupPlayer(PlayerGroupActive(), l);
                if (player == tmp_player) { continue; }

                if (!PlayerGetAlliance(player, c_allianceIdControl, tmp_player)) {
                    PlayerSetAlliance(player, c_allianceIdControl, tmp_player, true);
                    PlayerGroupAdd(pinactive_status[player].pg_shared_ctrl, tmp_player);
                }
            }
        }
    }
}

bool sf_afk_player_is_inactive(int p)
{
    return pinactive_status[p].inactive;
}
