struct sf_ctrl_zone_random_t {
    region area;
    trigger trig_periodic;
    fixed delay_min;
    fixed delay_max;
    fixed count_min;
    fixed count_max;
    fixed distance_min;
    fixed distance_max;
    fixed spread_min;
    fixed spread_max;
    fixed notify_delay;
    bool allow_interruption;
    bool must_look_target;
};

sf_ctrl_zone_random_t [SF_CTRL_CONTROLLER_MAX]sf_ctrl_zone_randoms;
int sf_ctrl_zone_random_tmp_cl_id = -1;
trigger sf_ctrl_zone_random_trig_idle = null;

point sf_ctrl_zone_radom_find_target(unit munit, structref<sf_ctrl_controller_t> ctl, structref<sf_ctrl_zone_random_t> zrn)
{
    int i;
    int tries;
    fixed best_min_distance;
    fixed best_max_distance;
    point bestPoint;
    fixed current_min_distance;
    fixed current_max_distance;
    point current_point;
    fixed current_unit_distance;
    fixed angle;
    unit tmp_unit;

    for (tries = 0; tries < 30; tries += 1) {
        // find current_point basing on current units position
        if (munit != null && zrn.distance_min != 0.0) {
            if (false && zrn.must_look_target) {
                angle = UnitGetFacing(munit);
            }
            else {
                angle = RandomFixed(0, 360.0);
            }
            current_point = PointWithOffsetPolar(
                UnitGetPosition(munit),
                RandomFixed(zrn.distance_min, zrn.distance_max),
                angle
            );
        }
        // or pick randomly if munit not provided
        else {
            current_point = RegionRandomPoint(zrn.area);
        }

        // check if current_point is winth area bounds
        if (!RegionContainsPoint(zrn.area, current_point)) { continue; }

        // check how close/far current_point is compared to other units within zone
        for (i = 1; i <= UnitGroupCount(ctl.ugroup, c_unitCountAll); i += 1) {
            tmp_unit = UnitGroupUnit(ctl.ugroup, i);
            if (tmp_unit == munit) { continue; }

            current_unit_distance = DistanceBetweenPoints(current_point, UnitGetPosition(tmp_unit));

            if (i == 1 || current_unit_distance < current_min_distance) {
                current_min_distance = current_unit_distance;
            }
            if (i == 1 || current_unit_distance > current_max_distance) {
                current_max_distance = current_unit_distance;
            }
        }

        // flag this point as best if it's better in terms of spread distance
        if (tries == 0 || (current_max_distance - current_min_distance) > (best_max_distance - best_min_distance)) {
            bestPoint = current_point;
            best_min_distance = current_min_distance;
            best_max_distance = current_max_distance;
        }

        // check if point is valid in terms of spread distance
        if (
            (zrn.spread_min == -1.0 || zrn.spread_min <= best_min_distance) &&
            (zrn.spread_max == -1.0 || zrn.spread_max >= best_max_distance)
        ) {
            break;
        }
    }

    if (bestPoint != null) {
        return bestPoint;
    }
    else {
        return RegionRandomPoint(zrn.area);
    }
}

void sf_ctrl_zone_random_cleanup(int cl_id)
{
    if (sf_ctrl_zone_randoms[cl_id].trig_periodic != null) {
        TriggerStop(sf_ctrl_zone_randoms[cl_id].trig_periodic);
        TriggerDestroy(sf_ctrl_zone_randoms[cl_id].trig_periodic);
        sf_ctrl_zone_randoms[cl_id].trig_periodic = null;
    }

    if (sf_ctrl_zone_random_trig_idle != null) {
        TriggerStop(sf_ctrl_zone_random_trig_idle);
        TriggerDestroy(sf_ctrl_zone_random_trig_idle);
        sf_ctrl_zone_random_trig_idle = null;
    }
}

int sf_ctrl_zone_random_create(
    region area,
    fixed delay_min, fixed delay_max,
    fixed count_min, fixed count_max,
    fixed distance_min, fixed distance_max,
    fixed spread_min, fixed spread_max,
    fixed notify_delay, bool allow_interruption, bool must_look_target
)
{
    structref<sf_ctrl_zone_random_t> zrn;
    int cl_id;

    cl_id = sf_ctrl_controller_create(SF_CTRL_TYPE_ZONE_RANDOM);
    zrn = sf_ctrl_zone_randoms[cl_id];

    zrn.area = area;
    zrn.delay_min = delay_min;
    zrn.delay_max = delay_max;
    zrn.count_min = count_min;
    zrn.count_max = count_max;
    zrn.distance_min = distance_min;
    zrn.distance_max = distance_max;
    zrn.spread_min = spread_min;
    zrn.spread_max = spread_max;
    zrn.notify_delay = notify_delay;
    zrn.allow_interruption = allow_interruption;
    zrn.must_look_target = must_look_target;

    if (zrn.distance_min == -1.0) {
        zrn.distance_min = 0.0;
    }
    if (zrn.distance_max == -1.0) {
        zrn.distance_max = DistanceBetweenPoints(RegionGetBoundsMin(zrn.area), RegionGetBoundsMax(zrn.area));
    }

    // Dbg("[zrandom/init] dist_min = " + FixedToString(zrn.distance_min, 2) + "; dist_max = " + FixedToString(zrn.distance_max, 2));

    sf_ctrl_zone_random_tmp_cl_id = cl_id;
    zrn.trig_periodic = TriggerCreate("sf_ctrl_zone_random_on_periodic");
    TriggerExecute(zrn.trig_periodic, false, false);

    if (sf_ctrl_zone_random_trig_idle == null) {
        sf_ctrl_zone_random_trig_idle = TriggerCreate("sf_ctrl_zone_random_on_idle");
    }

    return cl_id;
}

void sf_ctrl_zone_random_append_unit(int cl_id, unit munit, bool preserve_position)
{
    UnitGroupAdd(sf_ctrl_controllers[cl_id].ugroup, munit);
    if (!preserve_position) {
        UnitSetPosition(munit, sf_ctrl_zone_radom_find_target(null, sf_ctrl_controllers[cl_id], sf_ctrl_zone_randoms[cl_id]), false);
    }
    UnitSetFacing(munit, RandomFixed(0.0, 360.0), TICK_PERIOD);
    UnitSetCustomValue(munit, UNIT_CUSTOM_CONTROLLER_ID, cl_id + 1);
    TriggerAddEventUnitBecomesIdle(sf_ctrl_zone_random_trig_idle, UnitRefFromUnit(munit), true);
}

bool sf_ctrl_zone_random_on_periodic(bool test_conds, bool run_actions)
{
    int cl_id;
    structref<sf_ctrl_controller_t> ctl;
    structref<sf_ctrl_zone_random_t> zrn;
    unitgroup choosen_group = UnitGroupEmpty();
    unit tmp_unit;
    int choosen_count;
    int i;

    cl_id = sf_ctrl_zone_random_tmp_cl_id;
    ctl = sf_ctrl_controllers[cl_id];
    zrn = sf_ctrl_zone_randoms[cl_id];

    for (;;) {
        // random delay between phases
        Wait(RandomFixed(zrn.delay_min, zrn.delay_max), c_timeGame);

        // cleanup
        UnitGroupClear(choosen_group);

        // pick units matching conditions
        for (i = 1; i <= UnitGroupCount(ctl.ugroup, c_unitCountAll); i += 1) {
            tmp_unit = UnitGroupUnit(ctl.ugroup, i);

            // filter out units about to move
            if (UnitGetCustomValue(tmp_unit, UNIT_CUSTOM_ZONE_WARMUP) == 1.0) { continue; }

            // filter out units which are not idle
            if (!zrn.allow_interruption && !UnitTestState(tmp_unit, c_unitStateIdle)) { continue; }

            UnitGroupAdd(choosen_group, tmp_unit);
        }

        // pick right amount of units
        choosen_count = FixedToInt(Round(RandomFixed(zrn.count_min, zrn.count_max)));
        if (choosen_count > UnitGroupCount(choosen_group, c_unitCountAll)) {
            choosen_count = UnitGroupCount(choosen_group, c_unitCountAll);
        }
        while (choosen_count < UnitGroupCount(choosen_group, c_unitCountAll)) {
            UnitGroupRemove(choosen_group, UnitGroupRandomUnit(choosen_group, c_unitCountAll));
        }

        // prepare units to move
        for (i = 1; i <= choosen_count; i += 1) {
            tmp_unit = UnitGroupUnit(choosen_group, i);
            // targetPoints[i - 1] = zn_getRandomPoint(zone);
            UnitIssueOrder(tmp_unit, Order(AbilityCommand("stop", 0)), c_orderQueueReplace);

            if (zrn.notify_delay >= 0.0) {
                UnitSetCustomValue(tmp_unit, UNIT_CUSTOM_ZONE_WARMUP, 1.0);
                libNtve_gf_AttachActorToUnit(tmp_unit, "ObstacleWarmupNotify", "Origin");
            }

            // if (zrn.must_look_target) {
            //     UnitSetFacing(
            //         tmp_unit,
            //         AngleBetweenPoints(UnitGetPosition(tmp_unit), targetPoints[i - 1]),
            //         TICK_PERIOD
            //     );
            // }
        }

        Wait(zrn.notify_delay, c_timeGame);

        // time to go
        for (i = 1; i <= choosen_count; i += 1) {
            tmp_unit = UnitGroupUnit(choosen_group, i);

            UnitIssueOrder(
                tmp_unit,
                OrderTargetingPoint(AbilityCommand("move", 0), sf_ctrl_zone_radom_find_target(tmp_unit, ctl, zrn)),
                c_orderQueueReplace
            );
            UnitSetCustomValue(tmp_unit, UNIT_CUSTOM_ZONE_WARMUP, 0.0);
        }
    }

    return true;
}

bool sf_ctrl_zone_random_on_idle(bool test_conds, bool run_actions)
{
    // UnitSetFacing(EventUnit(), RandomFixed(0.0, 360.0), TICK_PERIOD * 2);

    return true;
}
