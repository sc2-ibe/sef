static unit [MAX_PLAYERS + 1]gv_ARTvortexes;

void sf_game_abil_init() {
    TriggerAddEventUnitAbility(TriggerCreate("ab_onAbilityShade"), null, AbilityCommand("Shade", 0), c_unitAbilStageComplete, true);
    TriggerAddEventUnitAbility(TriggerCreate("ab_onAbilityShadeSelfKill"), null, AbilityCommand("ShadeSelfKill", 0), c_unitAbilStageComplete, true);
    TriggerAddEventUnitAbility(TriggerCreate("ab_onAbilityShadeUse"), null, AbilityCommand("ShadeUse", 0), c_unitAbilStageComplete, true);

    // drone's abils
    // TriggerAddEventUnitAbility(TriggerCreate("ab_onAbilityReviveCoin"), null, AbilityCommand("ReviveCoin", 0), c_unitAbilStageComplete, true);

    TriggerAddEventUnitAbility(TriggerCreate("sf_game_on_abil_propel"), null, AbilityCommand("Propel", 0), c_unitAbilStageComplete, true);
    TriggerAddEventUnitAbility(TriggerCreate("sf_game_on_abil_boost"), null, AbilityCommand("PowerBoost", 0), c_unitAbilStageComplete, true);
    TriggerAddEventUnitAbility(TriggerCreate("sf_game_on_abil_art"), null, AbilityCommand("ART", 0), c_unitAbilStageComplete, true);
    TriggerAddEventUnitAbility(TriggerCreate("sf_game_on_abil_art_switch"), null, AbilityCommand("Switch", 0), c_unitAbilStageComplete, true);
    TriggerAddEventUnitAbility(TriggerCreate("sf_game_on_abil_throw_essence"), null, AbilityCommand("ThrowEssence", 0), c_unitAbilStageComplete, true);
}

void sf_game_abil_shade_remove(unit shadeUnit) {
    structref<HERO> hero;

    hero = gm_players[UnitGetOwner(shadeUnit)].hero;

    UnitBehaviorRemove(hero.mainUnit, "ShadeTimeout", 1);
    UnitAbilityShow(hero.mainUnit, "ShadeUse", false);
    UnitAbilityShow(hero.mainUnit, "Shade", true);
    UnitRemove(shadeUnit);
}

sf_phys_jump_info_t[MAX_PLAYERS + 1] sf_game_abil_shade_jumpinf;

bool ab_onAbilityShade(bool testConds, bool runActions) {
    structref<HERO> hero;

    hero = gm_players[UnitGetOwner(EventUnit())].hero;

    UnitCreate(
        1,
        UnitGetType(hero.mainUnit) + "Shadow",
        c_unitCreateIgnorePlacement,
        UnitGetOwner(hero.mainUnit),
        UnitGetPosition(hero.mainUnit),
        UnitGetFacing(hero.mainUnit)
    );
    UnitSetState(UnitLastCreated(), c_unitStateSelectable, false);
    UnitSetState(UnitLastCreated(), c_unitStateRadarable, false);
    UnitSetState(UnitLastCreated(), c_unitStateInvulnerable, true);
    UnitIssueOrder(UnitLastCreated(), Order(AbilityCommand("ShadeSelfKill", 0)), c_orderQueueReplace);
    UnitSetCustomValue(UnitLastCreated(), UNIT_CUSTOM_SHADE_ANGLE, UnitGetFacing(hero.guideUnit));
    hero.shadeUnit = UnitLastCreated();
    ActorSend(
        ActorFromScope(ActorScopeFromUnit(UnitLastCreated()), "::Main"),
        // "SetTintColor {255,6,6 2.5}"
        libNtve_gf_SetTintColor(libNtve_gf_ConvertPlayerColorToColor(PlayerGetColorIndex(UnitGetOwner(hero.mainUnit), false)), 2.5, 0.0)
    );
    UnitAbilityShow(hero.mainUnit, "ShadeUse", true);
    UnitAbilityShow(hero.mainUnit, "Shade", false);

    if (sf_phys_is_unit_during_jump(hero.mainUnit)) {
        UnitSetCustomValue(hero.shadeUnit, SF_GAME_UNIT_CUSTOM_IN_JUMP, 1.0);
        sf_phys_get_unit_jump_info(hero.mainUnit, sf_game_abil_shade_jumpinf[hero.player]);

        UnitSetState(UnitLastCreated(), c_unitStateIgnoreTerrainZ, false);
        UnitSetHeight(UnitLastCreated(), UnitGetHeight(hero.mainUnit) - WorldHeight(c_heightMapGround, UnitGetPosition(hero.mainUnit)), 0.0);
    }
    else {
        UnitSetState(UnitLastCreated(), c_unitStateIgnoreTerrainZ, false);
        UnitSetHeight(UnitLastCreated(), 0, 0.0);
    }

    UnitBehaviorAdd(hero.mainUnit, "ShadeTimeout", hero.mainUnit, 1);

    return true;
}

bool ab_onAbilityShadeSelfKill(bool testConds, bool runActions) {
    sf_game_abil_shade_remove(EventUnit());

    libNtve_gf_CreateModelAtPoint("HybridDAttackImpact", libNtve_gf_PointWithZOffset(UnitGetPosition(EventUnit()), 0.3));
    // libNtve_gf_SetAnimationTimeScale(libNtve_gf_ActorLastCreated(), c_animNameDefault, 3);
    libNtve_gf_KillModel(libNtve_gf_ActorLastCreated());

    return true;
}

bool ab_onAbilityShadeUse(bool testConds, bool runActions) {
    structref<HERO> hero;
    fixed fakeFacing;

    hero = gm_players[UnitGetOwner(EventUnit())].hero;

    if (!UnitIsAlive(hero.mainUnit)) {
        return false;
    }

    hero.paused = true;
    ActorSend(
        ActorFromScope(ActorScopeFromUnit(hero.mainUnit), "::Main"),
        "SetOpacity 0.0 " + FixedToString(1 * TICK_PERIOD, 4)
    );

    UnitSetState(hero.mainUnit, c_unitStateInvulnerable, true);
    libNtve_gf_CreateModelAtPoint("HybridDAttackImpact", libNtve_gf_PointWithZOffset(UnitGetPosition(EventUnit()), 0.3));
    // libNtve_gf_SetAnimationTimeScale(libNtve_gf_ActorLastCreated(), c_animNameDefault, 3);
    libNtve_gf_KillModel(libNtve_gf_ActorLastCreated());

    UnitIssueOrder(hero.shadeUnit, Order(AbilityCommand("stop", 0)), c_orderQueueReplace);

    UnitBehaviorRemove(hero.mainUnit, "ShadeTimeout", 1);
    ActorSend(
        ActorFromScope(ActorScopeFromUnit(hero.shadeUnit), "::Main"),
        "SetOpacity 1.0 " + FixedToString(3 * TICK_PERIOD, 4)
    );
    Wait(3 * TICK_PERIOD, c_timeGame);

    UnitSetHeight(hero.mainUnit, UnitGetHeight(hero.shadeUnit), 0.0);
    if (sf_phys_is_unit_during_jump(hero.shadeUnit)) {
        sf_phys_restore_unit_jump_info(hero.mainUnit, sf_game_abil_shade_jumpinf[hero.player]);
    }
    else if (sf_phys_is_unit_during_jump(hero.mainUnit)) {
        sf_phys_abort_unit_jump(hero.mainUnit);
    }

    hero.paused = false;
    fakeFacing = UnitGetFacing(hero.shadeUnit);
    UnitSetState(hero.mainUnit, c_unitStateInvulnerable, false);
    gm_setHeroPosition(hero, UnitGetPosition(hero.shadeUnit), UnitGetCustomValue(hero.shadeUnit, UNIT_CUSTOM_SHADE_ANGLE), 0);
    sf_game_abil_shade_remove(hero.shadeUnit);
    UnitSetFacing(hero.mainUnit, fakeFacing, 0.0);

    ActorSend(
        ActorFromScope(ActorScopeFromUnit(hero.mainUnit), "::Main"),
        "SetOpacity 1.0 " + FixedToString(0.0, 4)
    );

    SoundPlayAtPoint(SoundLink("PortalWarpOneWayArrive", -1), PlayerGroupAll(), UnitGetPosition(hero.mainUnit), 0.3, 170.0, 0.0);
    libNtve_gf_CreateActorAtPoint("ShadeImpact", UnitGetPosition(hero.mainUnit));

    return true;
}

bool ab_onAbilityReviveCoin(bool testConds, bool runActions) {
    structref<HERO> hero;
    unitgroup uGroup;
    unit missileUnit;
    point target;
    fixed range;
    fixed coinRadius;
    unit tempUnit;
    int i;

    hero = gm_players[UnitGetOwner(EventUnit())].hero;

    if (sf_phys_is_point_on_ice(UnitGetPosition(hero.mainUnit)) || sf_phys_is_unit_during_jump(hero.mainUnit)) {
        range = 0.0;
        if (UnitAbilityGetLevel(hero.mainUnit, "ReviveCoin") == 0) {
            range = 7.0;
        }
        else if (UnitAbilityGetLevel(hero.mainUnit, "ReviveCoin") == 1) {
            range = 11.0;
        }
        else if (UnitAbilityGetLevel(hero.mainUnit, "ReviveCoin") == 2) {
            range = 14.0;
        }
        target = PointWithOffsetPolar(
            UnitGetPosition(hero.mainUnit),
            range,
            UnitGetFacing(hero.guideUnit)
        );

        UnitCreateEffectPoint(
            hero.mainUnit,
            // "TempestLaunchMissile",
            "ReviveCoinLaunchMissile",
            target
        );
        uGroup = UnitGroupSearch(
            // "TempestWeapon",
            "ReviveCoin",
            UnitGetOwner(hero.mainUnit),
            UnitGetPosition(hero.mainUnit),
            0.0,
            UnitFilter(
                0,
                0,
                0,
                0
            ),
            1
        );

        if (UnitGroupCount(uGroup, c_unitCountAll) == 0) {
            // print("projectile not found");
            return false;
        }
        missileUnit = UnitGroupUnit(uGroup, 1);
        coinRadius = UnitGetPropertyFixed( missileUnit, c_unitPropRadius, true );

        for (;;) {
            if (!UnitIsValid(missileUnit) || !UnitIsAlive(missileUnit)) { break; }
            target = UnitGetPosition(missileUnit);

            uGroup = cl_getCollidingUnits(missileUnit, coinRadius);
            for (i = 1; i <= UnitGroupCount(uGroup, c_unitCountAll); i += 1) {
                tempUnit = UnitGroupUnit(uGroup, i);
                if (UnitGetCustomValue(tempUnit, UNIT_CUSTOM_COLLIDER_TYPE) == CL_TYPE_HERO_CORPSE) {
                    hr_revive(UnitGetOwner(hero.mainUnit), gm_players[UnitGetOwner(tempUnit)].hero, true);
                }
            }

            Wait(TICK_PERIOD, c_timeGame);
        }
    }
    else {
        target = UnitGetPosition(hero.mainUnit);
    }

    UnitCreate(
        1,
        "ReviveCoinEgg",
        c_unitCreateIgnorePlacement,
        UnitGetOwner(hero.mainUnit),
        target,
        90.0
    );
    Wait(1.5, c_timeGame);

    uGroup = cl_getCollidingUnits(UnitLastCreated(), 1.35);
    for (i = 1; i <= UnitGroupCount(uGroup, c_unitCountAll); i += 1) {
        tempUnit = UnitGroupUnit(uGroup, i);
        if (UnitGetCustomValue(tempUnit, UNIT_CUSTOM_COLLIDER_TYPE) == CL_TYPE_HERO_CORPSE) {
            hr_revive(UnitGetOwner(hero.mainUnit), gm_players[UnitGetOwner(tempUnit)].hero, true);
        }
    }

    SoundPlayAtPoint(SoundLink("Vile_Land", -1), PlayerGroupAll(), target, 0.0, 100.0, 0.0);
    UnitKill(UnitLastCreated());

    return true;
}

bool sf_game_on_abil_art(bool testConds, bool runActions) {
    structref<HERO> hero;

    hero = gm_players[UnitGetOwner(EventUnit())].hero;
    hero.artSwitch = false;
    hero.artActive = true;

    // UnitAbilityShow(hero.mainUnit, "ART", false);
    UnitAbilityShow(hero.mainUnit, "Switch", true);
    for (;;) {
        Wait(0.5 + TICK_PERIOD, c_timeGame);
        if (!UnitHasBehavior(hero.mainUnit, "ART")) { break; }
    }
    // UnitAbilityShow(hero.mainUnit, "ART", true);
    UnitAbilityShow(hero.mainUnit, "Switch", false);
    hero.artActive = false;

    return true;
}

bool sf_game_on_abil_art_switch(bool testConds, bool runActions) {
    structref<HERO> hero;

    hero = gm_players[UnitGetOwner(EventUnit())].hero;
    hero.artSwitch = !hero.artSwitch;
    if (hero.artSwitch) {
        ActorScopeSend(ActorScopeFromUnit(hero.mainUnit), "Signal {ARTSwitch}");
    }
    else {
        ActorScopeSend(ActorScopeFromUnit(hero.mainUnit), "Signal {ARTNormal}");
    }

    return true;
}

bool sf_game_on_abil_propel(bool testConds, bool runActions) {
    structref<HERO> hero;

    hero = gm_players[UnitGetOwner(EventUnit())].hero;
    hero.propeling = true;
    Wait(2.0, c_timeGame);
    hero.propeling = false;

    return true;
}

bool sf_game_on_abil_boost(bool testConds, bool runActions) {
    structref<HERO> hero;

    hero = gm_players[UnitGetOwner(EventUnit())].hero;
    libNtve_gf_SendActorMessageToUnit(EventUnit(), "SetTintColor {224,99,5 1.400000} 0.250000");
    Wait(10.0, c_timeGame);
    libNtve_gf_SendActorMessageToUnit(EventUnit(), "SetTintColor 255,255,255 0.250000");

    return true;
}

bool sf_game_on_abil_throw_essence(bool testConds, bool runActions) {
    structref<HERO> hero;
    int lv_n = UnitGetOwner(EventUnit());
    fixed lv_abilityRange;
    unit lv_thrownEssence;
    point lv_pointEssenceThrown_From;
    point lv_targetPointForTorus;
    fixed lv_angle;
    fixed auto154E3B59_at;
    unitgroup uGroup;
    unit tempUnit;
    int i;
    int l;
    sf_phys_jump_info_t jump_info;

    hero = gm_players[UnitGetOwner(EventUnit())].hero;

    lv_abilityRange = 5.0;
    if (UnitAbilityGetLevel(EventUnit(), "ThrowEssence") == 1) {
        lv_abilityRange = 8.0;
    }
    else if (UnitAbilityGetLevel(EventUnit(), "ThrowEssence") == 2) {
        lv_abilityRange = 12.0;
    }
    else if (UnitAbilityGetLevel(EventUnit(), "ThrowEssence") == 3) {
        lv_abilityRange = 14.0;
    }

    lv_angle = UnitGetFacing(hero.guideUnit);
    lv_pointEssenceThrown_From = UnitGetPosition(EventUnit());
    UnitCreate(1, "ShapeTorus", 0, lv_n, lv_pointEssenceThrown_From, lv_angle);
    lv_thrownEssence = UnitLastCreated();
    UnitSetPropertyFixed(lv_thrownEssence, c_unitPropHeight, UnitGetHeight(EventUnit()));
    UnitSetHeight(lv_thrownEssence, 0.0, 0.125);

    if (!sf_phys_is_land(hero.surfaceType, -1) || sf_phys_is_unit_during_jump(hero.mainUnit)) {
        if (sf_phys_is_unit_during_jump(hero.mainUnit)) {
            sf_phys_get_unit_jump_info(hero.mainUnit, jump_info);
            lv_angle = jump_info.landing_angle;
        }
        lv_targetPointForTorus = PointWithOffsetPolar(lv_pointEssenceThrown_From, lv_abilityRange, lv_angle);
        UnitSetCustomValue(lv_thrownEssence, 1, lv_abilityRange);
        UnitSetCustomValue(lv_thrownEssence, 2, lv_angle);
        UnitSetCustomValue(lv_thrownEssence, 3, PointGetX(lv_pointEssenceThrown_From));
        UnitSetCustomValue(lv_thrownEssence, 4, PointGetY(lv_pointEssenceThrown_From));
        UnitSetCustomValue(lv_thrownEssence, 5, PointGetX(lv_targetPointForTorus));
        UnitSetCustomValue(lv_thrownEssence, 6, PointGetY(lv_targetPointForTorus));
        UnitIssueOrder(lv_thrownEssence, OrderTargetingPoint(AbilityCommand("move", 0), lv_targetPointForTorus), c_orderQueueReplace);
        auto154E3B59_at = 0;
        while ((!(UnitGetPosition(lv_thrownEssence) == Point(UnitGetCustomValue(lv_thrownEssence, 5), UnitGetCustomValue(lv_thrownEssence, 6)))) && (auto154E3B59_at <= 20.0)) {
            Wait(0.0625, c_timeGame);
            auto154E3B59_at = auto154E3B59_at + 0.0625;

            uGroup = cl_getCollidingUnits(lv_thrownEssence, cl_getUnitRadius(lv_thrownEssence, 0.0));
            for (i = 1; i <= UnitGroupCount(uGroup, c_unitCountAll); i += 1) {
                tempUnit = UnitGroupUnit(uGroup, i);
                if (UnitGetCustomValue(tempUnit, UNIT_CUSTOM_COLLIDER_TYPE) == CL_TYPE_HERO_CORPSE) {
                    hr_revive(UnitGetOwner(hero.mainUnit), gm_players[UnitGetOwner(tempUnit)].hero, true);
                    sf_core_event_prepare("throw_essence_revive");
                    sf_core_event_set_int("player", hero.player);
                    sf_core_event_set_int("player_revived", UnitGetOwner(tempUnit));
                    sf_core_event_send();
                }
            }
        }
    }
    else {
        for (l = 0; l < FixedToInt(2.0 / TICK_PERIOD); l += 1) {
            uGroup = cl_getCollidingUnits(lv_thrownEssence, cl_getUnitRadius(lv_thrownEssence, 0.0));
            for (i = 1; i <= UnitGroupCount(uGroup, c_unitCountAll); i += 1) {
                tempUnit = UnitGroupUnit(uGroup, i);
                if (UnitGetCustomValue(tempUnit, UNIT_CUSTOM_COLLIDER_TYPE) == CL_TYPE_HERO_CORPSE) {
                    hr_revive(UnitGetOwner(hero.mainUnit), gm_players[UnitGetOwner(tempUnit)].hero, true);
                }
            }
            Wait(TICK_PERIOD, c_timeGame);
        }
    }
    if (UnitGetCustomValue(lv_thrownEssence, 0) != 1.0) {
        UnitRemove(lv_thrownEssence);
    }

    return true;
}
