static bool[MAX_PLAYERS + 1][IBE_REWARD_MAX] sfi_reward_player_unlocked_temp;

static void ibe_reward_save_to_stream(structref<db_stream_t> stream, structref<ibe_reward_state_t> rewp)
{
    int i;

    for (i = 0; i < IBE_REWARD_MAX; i += 1) {
        db_stream_push_bool(stream, rewp.unlocked[i]);
    }
    for (i = 0; i < IBE_REWARD_KIND_MAX; i += 1) {
        db_stream_push_int(stream, rewp.choice[i]);
    }
}

static void ibe_reward_load_from_stream(structref<db_stream_t> stream, structref<ibe_reward_state_t> rewp)
{
    int i;

    for (i = 0; i < IBE_REWARD_MAX; i += 1) {
        rewp.unlocked[i] = db_stream_pop_bool(stream);
    }
    for (i = 0; i < IBE_REWARD_KIND_MAX; i += 1) {
        rewp.choice[i] = db_stream_pop_int(stream);
    }
}

bool ibe_reward_state_migrate(structref<db_stream_t> stream, int from_ver)
{
    return false;
}

bool ibe_reward_state_load(int player)
{
    int version;
    string buff;
    db_stream_t stream;
    int tmp;

    sfi_reward_banks[player] = BankLoad(sfi_reward_bank_name, player);

    if (!BankSectionExists(sfi_reward_banks[player], "rewards")) {
        ibe_reward_state_save(player);
    }
    else {
        db_stream_init(stream, DB_STREAM_FIFO);

        buff = ibe_stat_bk_get_value_str(sfi_reward_banks[player], "rewards", "d");
        tmp = db_crypt_validate(buff);
        if (tmp != 0) {
            if (tmp == DB_CRYPT_ERR_CHECKSUM_MISSMATCH) {
                UIDisplayMessage(PlayerGroupSingle(player), c_messageAreaSubtitle, StringToText("Rewards data couldn't be loaded - checksum missmatch"));
            }
            else {
                Dbg("[reward/load] bank reset");
                ibe_reward_state_save(player);
            }
            return false;
        }

        db_stream_load(stream, db_crypt_decrypt(ibe_stat_player_encryption_key(player), buff));

        version = BankValueGetAsInt(sfi_reward_banks[player], "rewards", "v");
        if (version < IBE_REWARD_STATE_CURRENT_VERSION) {
            if (!ibe_reward_state_migrate(stream, version)) {
                ibe_reward_state_save(player);
                return false;
            }
            else {
            }
        }

        ibe_reward_load_from_stream(stream, sfi_reward_states[player]);
    }

    return true;
}

void ibe_reward_state_save(int player)
{
    db_stream_t stream;
    string buff;

    db_stream_init(stream, DB_STREAM_FIFO);

    if (!BankSectionExists(sfi_reward_banks[player], "rewards")) {
        BankSectionCreate(sfi_reward_banks[player], "rewards");
    }

    ibe_reward_save_to_stream(stream, sfi_reward_states[player]);
    buff = db_crypt_encrypt(ibe_stat_player_encryption_key(player), stream.buff);
    ibe_stat_bk_set_value_str(sfi_reward_banks[player], "rewards", "d", buff);
    BankValueSetFromInt(sfi_reward_banks[player], "rewards", "v", IBE_REWARD_STATE_CURRENT_VERSION);
    BankSave(sfi_reward_banks[player]);
}

void sfi_reward_init()
{
    int i;
    int l;

    for (i = 0; i < IBE_REWARD_MAX; i += 1) {
        sfi_rewards[i].kind = -1;
    }

    for (i = 1; i <= MAX_PLAYERS; i += 1) {
        for (l = 0; l < IBE_REWARD_KIND_MAX; l += 1) {
            sfi_reward_states[i].choice[l] = -1;
        }
    }
}

void sfi_reward_load()
{
    int player;
    int i;

    for (player = 1; player <= MAX_PLAYERS; player += 1) {
        // migrate data from profile
        for (i = 0; i < IBE_REWARD_MAX; i += 1) {
            sfi_reward_states[player].unlocked[i] = ibe_stat_profiles[player].rewards_unlocked[i];
        }
        for (i = 0; i < IBE_REWARD_KIND_MAX; i += 1) {
            sfi_reward_states[player].choice[i] = ibe_stat_profiles[player].rewards_choice[i];
        }
        ibe_reward_state_load(player);
    }
}

bool sfi_reward_register(int reward_id, int kind, bool exclusive, text title, text desc, string icon)
{
    sfi_rewards[reward_id].kind = kind;
    sfi_rewards[reward_id].exclusive = exclusive;
    sfi_rewards[reward_id].title = title;
    sfi_rewards[reward_id].desc = desc;
    if (icon != "") {
        sfi_rewards[reward_id].icon = icon;
    }
    else {
        sfi_rewards[reward_id].icon = "Assets\\Textures\\ui_ingame_help_techtree_questionmark.dds";
    }

    return true;
}

void sfi_reward_player_unlock_reward(int player, int reward_id)
{
    structref<ibe_reward_t> reward;

    reward = sfi_rewards[reward_id];

    Dbg("[reward/player_unlock_reward] p = " + IntToString(player) + " ; r_id = " + IntToString(reward_id) + " ; r_kind = " + IntToString(reward.kind));

    if (reward.kind == -1) {
        return;
    }

    if (!reward.exclusive) {
        if (!sfi_reward_states[player].unlocked[reward_id]) {
            TextExpressionSetToken("SF/Text/Reward_PlayerEarnedGlaze", "Player", sf_player_name_colored(player));
            TextExpressionSetToken("SF/Text/Reward_PlayerEarnedGlaze", "Reward", reward.title);
            UIDisplayMessage(
                PlayerGroupAll(),
                c_messageAreaSubtitle,
                TextExpressionAssemble("SF/Text/Reward_PlayerEarnedGlaze")
            );

            sf_core_event_prepare("reward_unlocked");
            sf_core_event_set_int("reward_id", reward_id);
            sf_core_event_set_int("reward_kind", reward.kind);
            sf_core_event_set_int("player", player);
            sf_core_event_send();

            sfi_reward_states[player].unlocked[reward_id] = true;
            ibe_reward_state_save(player);
        }

        if (sfi_reward_states[player].choice[reward.kind] == -1) {
            sfi_reward_player_activate_reward(player, reward_id);
        }
    }
    else {
        sfi_reward_player_unlocked_temp[player][reward_id] = true;
    }
}

bool sfi_reward_player_has_reward(int player, int reward_id)
{
    if (sfi_rewards[reward_id].exclusive) {
        Dbg("[reward/player_has_reward] p = " + IntToString(player) + " ; r_id = " + IntToString(reward_id) + " ; rval = " + libNtve_gf_ConvertBooleanToString(sfi_reward_player_unlocked_temp[player][reward_id]) + " [EXC]");
        return sfi_reward_player_unlocked_temp[player][reward_id];
    }
    else {
        Dbg("[reward/player_has_reward] p = " + IntToString(player) + " ; r_id = " + IntToString(reward_id) + " ; rval = " + libNtve_gf_ConvertBooleanToString(sfi_reward_states[player].unlocked[reward_id]) + " [NON-EXC]");
        return sfi_reward_states[player].unlocked[reward_id];
    }
}

int sfi_reward_player_get_active_reward(int player, int reward_kind)
{
    int reward_id;

    reward_id = sfi_reward_states[player].choice[reward_kind];

    Dbg("[reward/player_get_active] p = " + IntToString(player) + " ; r_kind = " + IntToString(reward_kind) + " ; r_id = " + IntToString(reward_id));

    if (reward_id == -1 || !sfi_reward_player_has_reward(player, reward_id)) {
        return -1;
    }

    return reward_id;
}

void sfi_reward_player_activate_reward(int player, int reward_id)
{
    int kind;

    if (!sfi_reward_player_has_reward(player, reward_id)) { return; }

    kind = sfi_rewards[reward_id].kind;

    if (sfi_reward_states[player].choice[kind] != reward_id) {
        sfi_reward_states[player].choice[kind] = reward_id;
    }
    else {
        sfi_reward_states[player].choice[kind] = -1;
    }
    ibe_reward_state_save(player);

    sf_core_event_prepare("reward_activation");
    sf_core_event_set_int("reward_id", reward_id);
    sf_core_event_set_int("reward_kind", kind);
    sf_core_event_set_int("player", player);
    sf_core_event_set_int("choice", sfi_reward_states[player].choice[kind]);
    sf_core_event_send();
}

void sfi_reward_add_event_activation(trigger trig)
{
    sf_core_event_attach(trig, "reward_activation");
}

void sfi_reward_add_event_unlocked(trigger trig)
{
    sf_core_event_attach(trig, "reward_unlocked");
}
